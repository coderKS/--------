#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pyquery import PyQuery as pq
import re
import urllib
import urllib2
import sys
import codecs

dictionary = dict() # global cache for dictionary
dictionary_path = "./data/dictionary"

def init():
	reload(sys)
	sys.setdefaultencoding('utf-8')

def isChineseChar(c):
	if u'\u4e00' <= c <= u'\u9fff':
		return True
	else:
		return False

def getLyrics(url,fileOutput):
	d = pq(url=url)
	data = d("dd#fsZx3").html().encode('utf-8')
	name = d("dt#fsZx2").html().encode('utf-8').lstrip(' ')

	# find lyrics start position
	startIndex = str(data).find('<br /><br />')
	endIndex = str(data).rfind('<br /><br />')
	print ("startIndex="+str(startIndex), " endIndex="+str(endIndex))
	if startIndex != endIndex and startIndex <= 300:
		# more than on <br /><br />
		lyrics_data = str(data)[startIndex+len("<br /><br />"):]
	else:
		# only one <br /><br />
		lyrics_data = str(data)
	# print "lyrics_data="+lyrics_data

	# split by <br />
	data2 = re.split('<br />',lyrics_data)
	bucket = []
	filtered = []

	# filter out empty and tag line
	for line in data2:
		if line == "":
			continue
		if line.startswith('['):
			continue
		if line.startswith('<'):
			continue
		if line.startswith('&'):
			continue
		bucket.append(line)

	# filter out first two and last line and remove <a> tag
	print (name+": "),
	# print data2
	spliters = ['　','，',' ']
	default_spliter = ' '
	fileOutput.write(name + ":")
	for i in xrange(len(bucket)):
		print "bucket="+bucket[i]
		if "<a" in bucket[i]:
			continue # skip advertisement
		for spliter in spliters:
			if len(bucket[i].split(spliter)) == 1 and spliter != default_spliter:
				continue
			print " <separate by [%s] > " % (spliter)
			for sentence in bucket[i].split(spliter):
				print "sentence="+sentence+" =>",
				if len(sentence) == 0:
					continue
				sen = sentence.decode('utf-8').rstrip(")'…。#@*、.＃＠＊）′")
				if len(sen) == 0:
					continue
				word = sen[len(sen)-1]
				print word
				if isChineseChar(word):
					fileOutput.write(word + " ")
			break # only perform once
		print ""
	print ""

def readDict():
	# read dictionary cache into global dictionary
	global dictionary
	global dictionary_path
	fileDictionary = open(dictionary_path)
	dictionary_data = fileDictionary.read()
	fileDictionary.close()
	if dictionary_data:
		dictionary = eval(dictionary_data)

def writeDict():
	# write back dictionary cache
	global dictionary
	global dictionary_path
	fileDictionary = codecs.open(dictionary_path,'w','utf-8')
	dictionary_data = str(dictionary)
	fileDictionary.write(dictionary_data)
	fileDictionary.close()

def getRhyme(url):
	try: 
		d = pq(url=url)
	except:
		print "Network Error!"
		writeDict()
		print "Retrying...",
		success = False
		for i in xrange(3):
			print "%d time" % (i+1)
			try:
				d = pq(url=url)
				success = True
				break
			except:
				print "Network Error!"
		if not success:
			sys.exit()

	prefix = d("td font[color=\"red\"]").html()
	if prefix:
		prefix = prefix.replace(' ','')
	rhyme = d("td font[color=\"green\"]").html().replace(' ','')
	pitch = d("td font[color=\"blue\"]").html().replace(' ','')

	return tuple((prefix, rhyme, pitch))

def toBig5(c):
	return urllib.quote(c.decode('utf8').encode('big5'))

def generateRhymes(fileInputPath, fileOutputPath):
	init()
	baseUrl = "http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/search.php?q="
	fileResult = open(fileInputPath)
	fileOutput = codecs.open(fileOutputPath,'w','utf-8')

	readDict()

	for line in fileResult:
		name, wordLine = line.split(':')
		print "parsing song: " + name
		words = wordLine.split()
		fileOutput.write(name + ":")
		for word in words:
			readableWord = word.encode('utf-8')
			print readableWord + " => ",
			if readableWord in dictionary:
				rhyme = dictionary[readableWord][1]
				fileOutput.write(rhyme + " ")
			else:
				result = getRhyme(baseUrl + toBig5(word))
				# print "tuple => " + str(result)
				rhyme = result[1]
				fileOutput.write(rhyme + " ")
				dictionary[readableWord] = result
			print rhyme + " "
		fileOutput.write("\n")

	writeDict()

def generateLyrics(inputFilePath, outputFilePath):
	init()
	fileUrl = open(inputFilePath)
	fileOutput = codecs.open(outputFilePath,'w','utf-8')

	for url in fileUrl:
		getLyrics(url,fileOutput)
		fileOutput.write("\n")

	fileUrl.close()
	fileOutput.close()
