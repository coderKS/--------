#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import codecs
import operator

def init():
	reload(sys)
	sys.setdefaultencoding('utf-8')

def isRhyme(rhymes_list, pos, rhyme_space):
	list_len = len(rhymes_list)
	is_match = False 
	for i in xrange(1, rhyme_space):
		if pos - i >= 0 and rhymes_list[pos - i] == rhymes_list[pos]:
			is_match = True
			break
		elif pos + i < list_len and rhymes_list[pos + i] == rhymes_list[pos]:
			is_match = True
			break
	# print "is_match=["+str(is_match)+"]",
	return is_match

def analyseRhymes(rhymes_path="", filtered_rhymes_path="", lyrics_path="", filtered_lyrics_path="", rhyme_space=3):
	init()

	# open file
	fileRhymes = open(rhymes_path)
	fileFilteredRhymes = codecs.open(filtered_rhymes_path,'w','utf-8')
	fileLyrics = open(lyrics_path)
	fileFilteredLyrics = codecs.open(filtered_lyrics_path,'w','utf-8')
	
	# read data into fileLyricsData and fileRhymesData
	fileLyricsData = []
	fileRhymesData = []
	for line in fileLyrics:
		fileLyricsData.append(line)
	for line in fileRhymes:
		fileRhymesData.append(line)

	# validation on line of data
	rhymesLineNum = len(fileRhymesData)
	lyricsLineNum = len(fileLyricsData)
	print "rhymesLineNum=%d" % (rhymesLineNum)
	print "lyricsLineNum=%d" % (lyricsLineNum)
	if rhymesLineNum != lyricsLineNum:
		print "Line number not equal"
		return

	# filter out all the non-rhymes and print it out
	for i in xrange(rhymesLineNum):
		name, rhymes_line = fileRhymesData[i].split(':')
		name, lyrics_line = fileLyricsData[i].split(':')

		rhymes = rhymes_line.rstrip(" ").split()
		lyrics = lyrics_line.rstrip(" ").split()

		rhymes_len = len(rhymes)
		lyrics_len = len(lyrics)

		print "rhymes_len=%d" % (rhymes_len)
		print "lyrics_len=%d" % (lyrics_len)
		if rhymes_len != lyrics_len:
			print "Invalid data"
			continue

		fileFilteredRhymes.write(name+":")
		fileFilteredLyrics.write(name+":")

		for j in xrange(rhymes_len):
			if isRhyme(rhymes, j, rhyme_space):
				fileFilteredRhymes.write(rhymes[j]+" ")
				fileFilteredLyrics.write(lyrics[j]+" ")

		fileFilteredRhymes.write("\n")
		fileFilteredLyrics.write("\n")

def analyseFreq(fileInputPath="", fileOutputPath=""):
	init()
	fileInput = open(fileInputPath)
	fileOutput = codecs.open(fileOutputPath,'w','utf-8')

	d = dict()
	for line in fileInput:
		name, wordLine = line.split(':')
		words = wordLine.rstrip(' \n').split()
		for word in words:
			if word in d:
				d[word] += 1
			else:
				d[word] = 1

	d2 = sorted(d.items(), key=operator.itemgetter(1), reverse=True)

	for data in d2:
		fileOutput.write(str(data[0]) + ": ")
		fileOutput.write(str(data[1])+"\n")
	
	fileInput.close()
	fileOutput.close()

def countWords(line):
	return len(line.rstrip(" \n").split())

def getDictInfo(d):
	d2 = sorted(d.items(), key=operator.itemgetter(1), reverse=True)
	return_str = ""
	for data in d2:
		return_str += str(data[0]) + "->"
		return_str += str(data[1]) + ","
	return return_str.rstrip(',')

def generateAnalyseReport( origin_rhymes="", filtered_lyrics="", filtered_rhymes="", fileOutputPath=""):
	init()
	fileOriginRhymes = open(origin_rhymes)
	fileFilteredLyrics = open(filtered_lyrics)
	fileFilteredRhymes = open(filtered_rhymes)
	fileOutput = codecs.open(fileOutputPath, 'w', 'utf-8')

	originRhymes = fileOriginRhymes.read().rstrip(" \n").split("\n")
	filteredLyrics = fileFilteredLyrics.read().rstrip(" \n").split("\n")
	filteredRhymes = fileFilteredRhymes.read().rstrip(" \n").split("\n")

	if not ( len(originRhymes) == len(filteredLyrics) == len(filteredRhymes) ):
		print ("Invalid data")
		return
	
	for i in xrange(len(originRhymes)):
		# calculate rhymes percentage
		name, originRhymesLine = originRhymes[i].split(':')
		name, filteredRhymeLine = filteredRhymes[i].split(':')
		name, filteredLyricLine = filteredLyrics[i].split(':')

		totalRhymes = countWords(originRhymesLine)
		realRhymes = countWords(filteredRhymeLine)
		print "totalRhymes=%d, realRhymes=%d" % (totalRhymes, realRhymes)
		rhymePercent = float(realRhymes) / float(totalRhymes)
		
		# calculate distinct rhymes 
		d = dict()
		rhymes = filteredRhymeLine.rstrip(" \n").split()
		for rhyme in rhymes:
			if rhyme in d:
				d[rhyme] += 1
			else:
				d[rhyme] = 1
		distinctRhymes = len(d)
		rhymesInfo = getDictInfo(d)	

		# calculate distinct words
		d = dict()
		lyrics = filteredLyricLine.rstrip(" \n").split()
		for lyric in lyrics:
			if lyric in d:
				d[lyric] += 1
			else:
				d[lyric] = 1
		distinctLyrics = len(d)
		lyricsInfo = getDictInfo(d)

		fileOutput.write(name+":")
		fileOutput.write(str(realRhymes)+" ")
		fileOutput.write(str(totalRhymes)+" ")
		fileOutput.write(str(rhymePercent)+" ")
		fileOutput.write(str(distinctRhymes)+" ")
		fileOutput.write(str(distinctLyrics)+" ")
		fileOutput.write(rhymesInfo+" ")
		fileOutput.write(lyricsInfo+" ")
		fileOutput.write("\n")





	


