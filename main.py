from generator import generateLyrics 
from generator import generateRhymes
from analyser import analyseRhymes
from analyser import analyseFreq
from analyser import generateAnalyseReport
from csvWriter import writeReport
from csvWriter import writeFreqRhymes
from csvWriter import writeFreqLyrics

old_songs_url_file_path = "data/old_songs_url"
new_songs_url_file_path = "data/new_songs_url"

old_songs_lyrics_file_path = "result/old_songs_lyrics"
new_songs_lyrics_file_path = "result/new_songs_lyrics"
old_songs_rhymes_file_path = "result/old_songs_rhymes"
new_songs_rhymes_file_path = "result/new_songs_rhymes"

old_songs_filtered_lyrics_file_path = "result/old_songs_filtered_lyrics"
new_songs_filtered_lyrics_file_path = "result/new_songs_filtered_lyrics"
old_songs_filtered_rhymes_file_path = "result/old_songs_filtered_rhymes"
new_songs_filtered_rhymes_file_path = "result/new_songs_filtered_rhymes"

old_songs_freq_lyrics = "result/old_songs_freq_lyrics"
new_songs_freq_lyrics = "result/new_songs_freq_lyrics"
old_songs_freq_rhymes = "result/old_songs_freq_rhymes"
new_songs_freq_rhymes = "result/new_songs_freq_rhymes"

old_songs_report = "result/old_songs_report"
new_songs_report = "result/new_songs_report"
old_songs_report_csv = "result/old_songs_report.csv"
new_songs_report_csv = "result/new_songs_report.csv"

old_songs_filtered_lyrics_csv = "result/old_songs_filtered_lyrics.csv"
new_songs_filtered_lyrics_csv = "result/new_songs_filtered_lyrics.csv"
old_songs_filtered_rhymes_csv = "result/old_songs_filtered_rhymes.csv"
new_songs_filtered_rhymes_csv = "result/new_songs_filtered_rhymes.csv"

######################## new #######################
generateLyrics(new_songs_url_file_path, new_songs_lyrics_file_path)
generateRhymes(new_songs_lyrics_file_path, new_songs_rhymes_file_path)

analyseRhymes(rhymes_path=new_songs_rhymes_file_path, 
				filtered_rhymes_path=new_songs_filtered_rhymes_file_path, 
 				lyrics_path=new_songs_lyrics_file_path, 
 				filtered_lyrics_path=new_songs_filtered_lyrics_file_path, 
 				rhyme_space=3)

analyseFreq(fileInputPath=new_songs_filtered_lyrics_file_path, 
			fileOutputPath=new_songs_freq_lyrics)

analyseFreq(fileInputPath=new_songs_filtered_rhymes_file_path, 
			fileOutputPath=new_songs_freq_rhymes)

generateAnalyseReport(origin_rhymes=new_songs_rhymes_file_path, 
					filtered_lyrics=new_songs_filtered_lyrics_file_path, 
					filtered_rhymes=new_songs_filtered_rhymes_file_path, 
					fileOutputPath=new_songs_report)

writeReport(new_songs_report, new_songs_report_csv)
writeFreqLyrics(new_songs_freq_lyrics, new_songs_filtered_lyrics_csv)
writeFreqRhymes(new_songs_freq_rhymes, new_songs_filtered_rhymes_csv)
######################################################

######################### old #######################
# generateLyrics(old_songs_url_file_path, old_songs_lyrics_file_path)
# generateRhymes(old_songs_lyrics_file_path, old_songs_rhymes_file_path)

# analyseRhymes(rhymes_path=old_songs_rhymes_file_path, 
# 				filtered_rhymes_path=old_songs_filtered_rhymes_file_path, 
#  				lyrics_path=old_songs_lyrics_file_path, 
#  				filtered_lyrics_path=old_songs_filtered_lyrics_file_path, 
#  				rhyme_space=3)

# analyseFreq(fileInputPath=old_songs_filtered_lyrics_file_path, 
# 			fileOutputPath=old_songs_freq_lyrics)

# analyseFreq(fileInputPath=old_songs_filtered_rhymes_file_path, 
# 			fileOutputPath=old_songs_freq_rhymes)

# generateAnalyseReport(origin_rhymes=old_songs_rhymes_file_path, 
# 					filtered_lyrics=old_songs_filtered_lyrics_file_path, 
# 					filtered_rhymes=old_songs_filtered_rhymes_file_path, 
# 					fileOutputPath=old_songs_report)
# writeReport(old_songs_report, old_songs_report_csv)
# writeFreqLyrics(old_songs_freq_lyrics, old_songs_filtered_lyrics_csv)
# writeFreqRhymes(old_songs_freq_rhymes, old_songs_filtered_rhymes_csv)
#######################################################

