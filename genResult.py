# For python version 3.5.2
#coding = utf-8
from colorama import init, Fore, Style
from pyquery import PyQuery as pq
from lxml import etree

init()

def findBig5(c):
    encoded = str(c.encode('big5')).replace('\\x','%').replace('\'','')[1:]
    # print('encoded: ',encoded)
    return encoded

def urlGen(c):
    url = 'http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/search.php?q=' + findBig5(c)
    # print('url: ',url)
    return url

def getData(c, num):
    if num == 1:
        num = 'td font[color=\"red\"]'
    elif num == 2:
        num = 'td font[color=\"green\"]'
    elif num == 3:
        num = 'td font[color=\"blue\"]'
    else:
        return False
    d = pq(url=urlGen(c))
    data = str(d(num).html()).replace(' ','')
    return data

while 1:
    var1 = input("請屌個中文字: ")
    print('音節: ' + Fore.RED + getData(var1, 1) + Fore.GREEN +  getData(var1, 2) + Fore.BLUE +  getData(var1, 3))
    print(Style.RESET_ALL, end='')
