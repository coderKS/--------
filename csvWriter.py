#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import codecs
import csv

def init():
	reload(sys)
	sys.setdefaultencoding('utf-8')

def writeFreqRhymes(fileInputPath, fileOutputPath):
	init()
	datafile = open(fileInputPath)
	csvfile = codecs.open(fileOutputPath,'w','utf-8')

	fieldnames = ['韻母','次數']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()

	for line in datafile:
		rhyme, freq = line.split(':')
		freq = freq.lstrip(' ').rstrip("\n")

		writer.writerow({
						'韻母': rhyme,
						'次數': freq
						})


def writeFreqLyrics(fileInputPath, fileOutputPath):
	init()
	datafile = open(fileInputPath)
	csvfile = codecs.open(fileOutputPath,'w','utf-8')
	dictionaryfile = open("./data/dictionary")
	dictionary_data = dictionaryfile.read()
	if dictionary_data:
		dictionary = eval(dictionary_data)
	else:
		dictionary = dict()

	fieldnames = ['常用押韻字','聲母','韻母','音調','次數']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()

	for line in datafile:
		word, freq = line.split(':')
		freq = freq.lstrip(' ').rstrip("\n")
		prefix = None
		rhyme = None
		pitch = None
		if word in dictionary:
			prefix = dictionary[word][0]
			rhyme = dictionary[word][1]
			pitch = dictionary[word][2]
		writer.writerow({'常用押韻字': word,
						'聲母': prefix if prefix else "N/A",
						'韻母': rhyme if rhyme else "N/A",
						'音調': pitch if pitch else "N/A",
						'次數': freq
						})

def writeReport(fileInputPath, fileOutputPath):
	init()
	datafile = open(fileInputPath)
	csvfile = codecs.open(fileOutputPath,'w','utf-8')
	fieldnames = ['歌名', '押韻字數','總句數','押韻百分比','不同韻母的數量','不同押韻字的數量','韻母資料','押韻字資料']

	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()

	for line in datafile:
		name, data_line = line.split(':')
		data = data_line.split()
		data_len = len(data)
		if data_len !=5 and data_len !=7:
			print "data error"
			continue
		writer.writerow({'歌名': name,
						 '押韻字數': data[0],
						 '總句數': data[1],
						 '押韻百分比': data[2],
						 '不同韻母的數量': data[3],
						 '不同押韻字的數量': data[4],
						 '韻母資料': data[5] if data_len == 7 else "",
						 '押韻字資料': data[6] if data_len == 7 else ""
						 })

